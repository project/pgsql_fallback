<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\Upsert as CoreUpsert;

/**
 * PostgreSQL implementation of \Drupal\Core\Database\Query\Upsert.
 */
class Upsert extends CoreUpsert {}
