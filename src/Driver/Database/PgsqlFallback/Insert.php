<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\Insert as CoreInsert;

/**
 * PostgreSQL implementation of \Drupal\Core\Database\Query\Insert.
 */
class Insert extends CoreInsert {}
