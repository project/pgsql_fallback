<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\NativeUpsert as CoreNativeUpsert;

/**
 * PostgreSQL implementation of native \Drupal\Core\Database\Query\Upsert.
 */
class NativeUpsert extends CoreNativeUpsert {}
