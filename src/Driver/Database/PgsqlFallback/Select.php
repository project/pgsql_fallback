<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\Select as CoreSelect;

/**
 * PostgreSQL implementation of \Drupal\Core\Database\Query\Select.
 */
class Select extends CoreSelect {}
