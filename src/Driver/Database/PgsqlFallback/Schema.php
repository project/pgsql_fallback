<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\Schema as CoreSchema;

/**
 * PostgreSQL implementation of \Drupal\Core\Database\Schema.
 */
class Schema extends CoreSchema {}
