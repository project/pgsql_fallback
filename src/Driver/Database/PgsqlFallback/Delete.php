<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback;

use Drupal\Core\Database\Driver\pgsql\Delete as CoreDelete;

/**
 * PostgreSQL implementation of \Drupal\Core\Database\Query\Delete.
 */
class Delete extends CoreDelete {}
