<?php

namespace Drupal\pgsql_fallback\Driver\Database\PgsqlFallback\Install;

use Drupal\Core\Database\Driver\pgsql\Install\Tasks as CoreTasks;

/**
 * Specifies installation tasks for PostgreSQL databases.
 */
class Tasks extends CoreTasks {

  /**
   * {@inheritdoc}
   */
  public function name() {
    return t('PostgreSQL Fallback');
  }

  /**
   * {@inheritdoc}
   */
  public function minimumVersion() {
    return '9.6';
  }

}
