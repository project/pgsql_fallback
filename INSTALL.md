# How to install this and any other contrib database drivers

Copy the `drivers` directory and everything that is in it, to the root directory of your Drupal project/installation (DRUPAL_ROOT). The root directory of your Drupal project/installation is usually called `drupal`. That directory has the following subdirectories: `composer`, `core`, `modules`, `profiles`, `sites`, `themes`  and `vendor`. This must be done, because that is the only location where Drupal looks for contrib database drivers.

Every time a contrib database driver you are using is updated, you must make sure that copy the contents of the `drivers` to the root directory of your Drupal project/installation. If you do not do that, you have not updated your contrib database driver. You will be still using the old version.

A contrib database driver can have two parts: a driver directory and a module part. At the moment this contrib database drivers only has the driver directory part. When the by core supported database driver for PostgreSQL has started using the [`pg_trgm`](https://www.postgresql.org/docs/current/pgtrgm.html) extension, this contrib database driver will also get a module part.

If you are updating this contrib database driver after it has a module part and you do not copy the contents of the `drivers` to the root directory of your Drupal project/installation, you will get some strange bugs. This is because the driver directory is on a different version then the module parts of the contrib database driver.
